# Ensono General Utilities for Windows Servers

WIP

## Usage

| Var       | value                                           |
|---------- |------------------------------------------------ |
| ensaction | ping - runs ansible ping against the target     |
|           | reboot - Reboots the  target server             |
| hostsedit | add - Adds host to Host(s) file                 |
|           | remove - removes host(s) to Host file           |
|           | RESET - Resets host file to it's intial state   |

