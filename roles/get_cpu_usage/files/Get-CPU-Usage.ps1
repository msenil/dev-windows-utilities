<#.SynopsisGet-CPU-Usage to get top 10 CPU utilizing processess.DESCRIPTIONThis script used to do below functionalities1.) To Get the Top 10 process utilizing the CPU from Altiris

.EXAMPLE.\Get-CPU-Usage.ps1Get the Top 5 process utilizing the CPU from Altiris

.NOTESHistory :Date			Version				Developer				Changes----			-------				---------				-------06/23/2017		1.0					Bakkiyaraj				Initial Realease

#>

#Set the Console ConfigurationClear-Hostmode con: cols=250 lines=900

#$ErrorActionPreference = "SilentlyContinue"$ComputerName = $env:COMPUTERNAME$UserName = $env:UserName$OSName = (Get-WmiObject Win32_OperatingSystem).Caption$OSVersion = (Get-WmiObject Win32_OperatingSystem).version$today_date = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"

$sheader_line = "".ToString().PadLeft(80, '-')
$header_line = "".ToString().PadLeft(150, '-')
#Clear-Host
write-host $header_linewrite-host "							CPU Utilizing Report"
write-host $header_linewrite-host " Started		: $today_date "
write-host " Machine Name		: $ComputerName "
write-host " OS Name		: $OSName "
write-host " OS Version		: $OSVersion "
write-host " Logged On User		: $UserName "
write-host $header_line

Function WMIDateStringToDateTime( [String] $strWmiDate ) 
{
$strWmiDate.Trim() > $null
$iYear   = [Int32]::Parse($strWmiDate.SubString( 0, 4))
$iMonth  = [Int32]::Parse($strWmiDate.SubString( 4, 2)).ToString().PadLeft(2, '0')
$iDay    = [Int32]::Parse($strWmiDate.SubString( 6, 2)).ToString().PadLeft(2, '0')
$iHour   = [Int32]::Parse($strWmiDate.SubString( 8, 2)).ToString().PadLeft(2, '0')
$iMinute = [Int32]::Parse($strWmiDate.SubString(10, 2)).ToString().PadLeft(2, '0')
$iSecond = [Int32]::Parse($strWmiDate.SubString(12, 2)).ToString().PadLeft(2, '0')

return "$iMonth\$iDay\$iYear ${iHour}:${iMinute}:${iSecond}"

}

try {
#Getting Processor Information
$ProcessorInfo = Get-WmiObject Win32_Processor -Filter "Role='CPU'"
if($ProcessorInfo.Count -gt 0) 
{
	foreach ($p in $ProcessorInfo) 
	{
		$AddressWidth = [string]$p.AddressWidth + "-bit"
		$DataWidth = [string]$p.DataWidth + "-bit"
		$CClockSpeed = [string]$p.CurrentClockSpeed + " MHz"
		$MClockSpeed = [string]$p.MaxClockSpeed + " MHz"
		$ProcessorCache = "0 MB"
		if($p.L2CacheSize -ne 0) 
		{
			$ProcessorCache = [string][math]::Round(($p.L2CacheSize/1024),0) + " MB"
		}

		$p | Add-Member -type NoteProperty -name PAddressWidth -value $AddressWidth
		$p | Add-Member -type NoteProperty -name PDataWidth -value $DataWidth
		$p | Add-Member -type NoteProperty -name PCClockSpeed -value $CClockSpeed
		$p | Add-Member -type NoteProperty -name PMClockSpeed -value $MClockSpeed
		$p | Add-Member -type NoteProperty -name ProcessorCache -value $ProcessorCache
	}
	
	write-host ""
	write-host ""
	write-host " $sheader_line"
	write-host "`t`t`t	Processor Information"
	$format = @{Expression={$_.Name};Label="Processor Name";width=60}, `
	#@{Expression={$_.Description};Label="Description";width=30}, `
	@{Expression={$_.PAddressWidth};Label="Address Width";width=13}, `
	@{Expression={$_.PDataWidth};Label="Data Width";width=10}, `
	@{Expression={$_.NumberOfLogicalProcessors};Label="Processors";width=10}, `
	@{Expression={$_.NumberOfCores};Label="Cores";width=6}, `
	@{Expression={$_.ProcessorCache};Label="Cache";width=8}, `
	@{Expression={$_.PMClockSpeed};Label="Max Speed";width=10}, `
	@{Expression={$_.PCClockSpeed};Label="Cur Speed";width=10}
	write-host " $sheader_line"
	#$ProcessorInfo | Format-Table $format -wrap
	write-host "  Processor Name, Address Width, Data Width, Processors, Cores, Cache, Max Speed, Current Speed"
	foreach ($pObj in $ProcessorInfo) {
		$Name = $pObj.Name
		$Description = $pObj.Description
		$AddressWidth = $pObj.PAddressWidth
		$DataWidth = $pObj.PDataWidth
		$NumberOfLogicalProcessors = $pObj.NumberOfLogicalProcessors
		$NumberOfCores = $pObj.NumberOfCores
		$ProcessorCache = $pObj.ProcessorCache
		$MClockSpeed = $pObj.PMClockSpeed
		$CClockSpeed = $pObj.PCClockSpeed
		write-host "  $Name, $AddressWidth, $DataWidth, $NumberOfLogicalProcessors, $NumberOfCores, $ProcessorCache, $MClockSpeed, $CClockSpeed"
	}
	write-host " $sheader_line"
}elseif($ProcessorInfo) {
	$Name = $ProcessorInfo.Name
	$Description = $ProcessorInfo.Description
	$AddressWidth = [string]$ProcessorInfo.AddressWidth + "-bit"
	$DataWidth = [string]$ProcessorInfo.DataWidth + "-bit"
	$LogicalProcessors = $ProcessorInfo.NumberOfLogicalProcessors
	$Cores = $ProcessorInfo.NumberOfCores
	$Status = $ProcessorInfo.Status
	$CClockSpeed = [string]$ProcessorInfo.CurrentClockSpeed + " MHz"
	$MClockSpeed = [string]$ProcessorInfo.MaxClockSpeed + " MHz"
	$ProcessorCache = "0 MB"
	if($ProcessorInfo.L2CacheSize -ne 0) {
		$ProcessorCache = [string][math]::Round(($ProcessorInfo.L2CacheSize/1024),0) + " MB"
	}
	
	write-host ""
	write-host ""
	write-host " $sheader_line"
	write-host " `t`t`t	Processor Information"
	write-host " $sheader_line"
	write-host "  Name			: $Name "
	write-host "  Description		: $Description "
	write-host "  AddressWidth		: $AddressWidth "
	write-host "  DataWidth		: $DataWidth "
	write-host "  No. Of Processors	: $LogicalProcessors "
	write-host "  No. Of Cores		: $Cores "
	write-host "  Cache			: $ProcessorCache "
	write-host "  Maximum Speed		: $MClockSpeed "
	write-host "  Current Speed		: $CClockSpeed "
	write-host "  Status		: $Status "
	write-host " $sheader_line"
}
 
#Getting Top 10 CPU Utilizing process
$CPUUtilization = Get-WmiObject Win32_PerfFormattedData_PerfProc_Process | ?{ $_.Name -ne "_Total" -and $_.Name -ne "Idle"} | Sort-Object PercentProcessorTime -Descending | select -First 10 | select IDProcess, Name, PercentProcessorTime
foreach ($p in $CPUUtilization) {
	$Name = $p.Name
	
	$getOwner = (Get-WmiObject -class win32_process | where{$_.ProcessID -eq $p.IDProcess}).getowner()
	$user = $getOwner.user
	if($user -eq $null) {
		$user="SYSTEM" 
	}
	$domain = $getOwner.domain
	if($domain -eq $null) {
		$domain="SYSTEM" 
	}
	$StartTime = Get-WmiObject -class win32_process | where{$_.ProcessID -eq $p.IDProcess} | Select -ExpandProperty CreationDate
	$StartTime = WMIDateStringToDateTime -strWmiDate $StartTime
	
	$gp = get-process -id $p.IDProcess | Select ID, Name, Responding, StartTime, Description
	
	$p | Add-Member -type NoteProperty -name User -value $user
	$p | Add-Member -type NoteProperty -name Domain -value $domain
	$p | Add-Member -type NoteProperty -name Description -value $gp.Description
	$p | Add-Member -type NoteProperty -name Responding -value $gp.Responding
	$p | Add-Member -type NoteProperty -name StartTime -value $StartTime
}
$TotalProcesses = (Get-WmiObject -class win32_process).Count
$TCPUUtilization= "{0:N2}" -f (get-counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1 -MaxSamples 5 | select -ExpandProperty countersamples | select -ExpandProperty cookedvalue | Measure-Object -Average).average
	
write-host ""
write-host ""
write-host " $sheader_line"
Write-Host "  TOTAL PROCESSES: $TotalProcesses"
Write-Host "  TOTAL CPU UTILIZATION: ${TCPUUtilization}%"
write-host " $sheader_line"
$format = @{Expression={$_.IDProcess};Label="Process ID";width=15}, `
@{Expression={$_.Name};Label="Process Name";width=25}, `
@{Expression={$_.StartTime};Label="Started";width=20}, `
@{Expression={$_.User};Label="User";width=25}, `
@{Expression={$_.Domain};Label="Domain";width=15}, `
@{Expression={$_.PercentProcessorTime};Label="CPU (%)";width=8}, `
@{Expression={$_.Responding};Label="Responding";width=10}, `
@{Expression={$_.Description};Label="Description";width=40}

write-host ""
write-host "  TOP 10 PROCESSES BY CPU"
write-host "  -----------------------"
#$CPUUtilization | Format-Table $format -wrap
write-host "   CPU(%), Process Name, Process ID, Started, User, Domain, Responding, Description"
foreach ($utilObj in $CPUUtilization) {
	$ProcessID = $utilObj.IDProcess
	$Name = $utilObj.Name
	$Description = $utilObj.Description
	$StartTime = $utilObj.StartTime
	$User = $utilObj.User
	$Domain = $utilObj.Domain
	$PercentProcessorTime = [string]$utilObj.PercentProcessorTime + "%"
	$Responding = $utilObj.Responding
	write-host "   $PercentProcessorTime, $Name, $ProcessID, $StartTime, $User, $Domain, $Responding, $Description"
}
write-host ""
write-host ""
write-host $header_line
$today_date = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"
Write-Host " Completed at $today_date"
write-host $header_line

}
catch 
{
$ErrorType = $.Exception.GetType
$ErrorMessage = $.Exception.Message
Write-Error -Message "[${ErrorType}]: $ErrorMessage "exit(1)
}