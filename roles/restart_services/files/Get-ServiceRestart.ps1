<#
.Synopsis
Get-ServiceRestart to restart service provided by user

.DESCRIPTION
This script used to do below functionalities
1.) To restart service

.EXAMPLE
.
Get-ServiceRestart.ps1
Local - Temp file Clean Up and No Email & Report
.NOTES
Date Version Developer Changes
---- ------- --------- -------
07/03/2017 1.0 Bakkiyaraj Initial Realease

#>

#$ErrorActionPreference = "SilentlyContinue"
$ComputerName = $env:COMPUTERNAME
$UserName = $env:UserName
$OSName = (Get-WmiObject Win32_OperatingSystem).Caption
$OSVersion = (Get-WmiObject Win32_OperatingSystem).version
$OSBit = (Get-WmiObject Win32_OperatingSystem).OSArchitecture
$today_date = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"

Function CheckService-Status {
param($Service,$State)

$maxRepeat = 15
do 
{
	$sRtn = Get-WmiObject win32_service | where-object {$_.Name -eq $Service}

	$maxRepeat--
	sleep -Milliseconds 30
}until ($State -eq $sRtn.state -or $maxRepeat -eq 0)

return $sRtn
}

Function message_exitfail {
param($message)

Write-Host ""
write-host "	$message"
write-host ""
write-host "---------------------------------------------------------------------------------------------------------------" 
$today_date = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"
Write-Host "	Completed at $today_date"
Write-host "---------------------------------------------------------------------------------------------------------------" 
exit(1)
}

clear
write-host "---------------------------------------------------------------------------------------------------------------"
write-host " Service Status & Start if stopped"
write-host "---------------------------------------------------------------------------------------------------------------"
write-host " Started : $today_date "
write-host " Machine Name : $ComputerName "
write-host " OS Name : $OSName "
write-host " OS Version : $OSVersion "
write-host " OS Bit(s) : $OSBit"
write-host " Logged On User : $UserName "
write-host "---------------------------------------------------------------------------------------------------------------"

try {

$ServiceName = $args[0]
if($ServiceName -eq $null -or $ServiceName -eq "") {
	Write-Host ""

	Write-Host "	Input Service Name Can't be empty`n"
	write-host "---------------------------------------------------------------------------------------------------------------" 
	exit(1)
}

$GetServiceErr = $null
$Service = Get-Service -Name $ServiceName -ErrorVariable GetServiceErr
echo "Get-Service -Name $ServiceName -ErrorVariable GetServiceErr"
$ServiceDisplayName = $Service.DisplayName
if(($Service) -or ($GetServiceErr -eq $null)) {
	write-host ""
	write-host ""
	write-host "Checking '${ServiceName}' Service"
	write-host "---------------------------------"
	
	$ServiceStatus = $Service.Status
	if( $ServiceStatus -eq "Running" ) {
		write-host ""
		write-host "	Service '${ServiceName} - ${ServiceDisplayName}' is in '${ServiceStatus}' Status"
	}
	
	#If Service stopped
	elseif ( $ServiceStatus -eq "Stopped" ) {
		write-host ""
		write-host ""
		write-host "	Service '${ServiceName} - ${ServiceDisplayName}' in 'Stopped' Status"
		write-host ""
		$GetStartServiceErr = $null
		$GetServiceErr = $null
		Start-Service -Inputobject (Get-Service -Name $Service.Servicename -ErrorVariable GetServiceErr) -ErrorVariable GetStartServiceErr
		if ($GetServiceErr -ne $null -or $GetStartServiceErr -ne $null)  {
			if($GetServiceErr) {
				$error_message = $GetServiceErr
			}
			if($GetStartServiceErr) {
				$error_message += $GetStartServiceErr
			}

			message_exitfail -message "		UNSUCCESSFUL: Service '${ServiceName}' was not restarted `n        ERROR: $error_message"
		}else {
			#Check if the Service successfully running or not
			$chk_rtn = CheckService-Status -Service $Service.Servicename -State "Running"
			$cur_state = $chk_rtn.state
			if($cur_state -ne "Running") {
				message_exitfail -message "		UNSUCCESSFUL: Service '${ServiceName}' was unable to restarted. Currently it is in '${cur_state}'"
			}else {
				write-host "		SUCCESSFULY: Service '${ServiceName}' was restarted"
			}
		}
	}
}else {
	message_exitfail -message "	Service '${ServiceName}' not available."
}
write-host ""
write-host ""
write-host "---------------------------------------------------------------------------------------------------------------" 
$today_date = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"
Write-Host "	Completed at $today_date"
Write-host "---------------------------------------------------------------------------------------------------------------" 
}catch {
$ErrorType = $.Exception.GetType
$ErrorMessage = $.Exception.Message
Write-Error -Message "[${ErrorType}]: $ErrorMessage "
exit(1)
}
